import {Component} from '@angular/core';
import {StorageService} from '../../services/starage.service';

@Component({
  selector: 'app-admin',
  templateUrl: 'admin.page.html',
  styleUrls: ['admin.page.css']
})
export class AdminPage {

  constructor(private storageService: StorageService){

  }

  logOut(){
    this.storageService.clear();
  }
}
