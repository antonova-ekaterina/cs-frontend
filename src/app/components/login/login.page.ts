import {Component} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {StorageService} from '../../services/starage.service';
import {Router} from '@angular/router';
import {AdminService} from '../../services/admin.service';

@Component({
  selector: 'app-login',
  templateUrl: 'login.page.html',
  styleUrls: ['login.page.css']
})
export class LoginPage {
  hide = true;
  isAdmin = false;
  user: FormGroup;
  errMessage: string = null;

  constructor(private fb: FormBuilder,
              private adminService: AdminService,
              private storageService: StorageService,
              private router: Router) {
    this.checkToken();
    this.user = fb.group({
      email: '',
      password: '',
    });
  }

  checkToken() {
    if (this.storageService.getItem('token')) {
      this.router.navigate(['/admin']);
    }
  }

  login() {
    const email = this.user.getRawValue().email;
    const password = this.user.getRawValue().password;
    this.adminService.login(email, password)
      .subscribe(token => {
          if (token) {
            this.storageService.setItem('token', token);
            return this.router.navigate(['/admin']);
          }
          this.errMessage = 'Логин или пароль не совпали';
        },
        () => this.router.navigate(['/']));
  }
}
