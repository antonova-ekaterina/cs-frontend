import {Component, OnInit} from '@angular/core';
import {ICountry} from '../../models/country';
import {MainService} from '../../services/main.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  selectedYear = 2016;
  placeholder = '2016';
  countries: ICountry[] = [];
  currentCountry: ICountry = null;
  currentRating: any = {
    discriminant: '',
    neuralNetworkSNN: '',
    neuralNetworkMatlab: '',
    fuzzy: ''
  };

  rating: string[] = ['D', 'DD', 'DDD', 'C', 'CC', 'CCC', 'B', 'BB', 'BBB', 'A', 'AA', 'AAA'];

  constructor(private mainService: MainService) {
  }

  ngOnInit() {
    this.mainService.getCountries(this.selectedYear)
      .subscribe((countries) => {
        this.countries = countries;
      });
  }

  setCountry(country) {
    this.currentCountry = country;
    const offRating = this.currentCountry.officialRating;
    this.currentRating.discriminant = this.compare(offRating, this.currentCountry.discriminantRating);
    this.currentRating.neuralNetworkSNN = this.compare(offRating, this.currentCountry.neuralNetworkSNNRating);
    this.currentRating.neuralNetworkMatlab = this.compare(offRating, this.currentCountry.neuralNetworkMatlabRating);
    this.currentRating.fuzzy = this.compare(offRating, this.currentCountry.fuzzyRating);
  }

  findCountryForName(name) {
    return this.countries.filter(country => {
      return country.name === name;
    })[0];
  }

  compare(rating1, rating2) {
    const index1 = this.rating.indexOf(rating1);
    const index2 = this.rating.indexOf(rating2);

    if (index1 === index2) {
      return 'ОЦЕНКА ОБЪЕКТИВНА';
    }

    if (index1 > index2) {
      return 'ПЕРЕОЦЕНЕНА';
    }
    if (index1 < index2) {
      return 'НЕДООЦЕНЕНА';
    }
  }

  changeYear(year) {
    this.selectedYear = year;
    this.placeholder = 'выберете год';
    this.mainService.getCountries(this.selectedYear)
      .subscribe((countries) => {
        this.countries = countries;
        this.currentCountry = this.currentCountry ? this.findCountryForName(this.currentCountry.name) : null;
      });
  }

}
