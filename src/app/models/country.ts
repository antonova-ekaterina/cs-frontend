export interface ICountry {
  id: number;
  name: string;
  year: string;
  x1: string;
  x2: string;
  x3: string;
  x4: string;
  x5: string;
  x6: string;
  officialRating: string;
  discriminantRating: string;
  neuralNetworkSNNRating: string;
  neuralNetworkMatlabRating: string;
  fuzzyRating: string;
}
