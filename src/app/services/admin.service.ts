import {Injectable} from '@angular/core';
import {HttpService} from './http.service';
import {map} from 'rxjs/operators';

@Injectable()
export class AdminService {

  constructor(private httpService: HttpService) {
  }

  login(email: string, password: string) {
    const body = {
      email: email,
      password: password
    };
    return this.httpService.post('admin/login', body)
      .pipe(
        map((req) => req.success ? req.data.token : false)
      );
  }
}
