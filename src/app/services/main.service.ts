import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpService} from './http.service';
import {ICountry} from '../models/country';
import {map} from 'rxjs/operators';

@Injectable()
export class MainService {
  constructor(private httpService: HttpService) {
  }

  getAll(): Observable<ICountry[]> {
    return this.httpService.get('country')
      .pipe(
        map(res => {
          if (res.success) {
            return res.data;
          } else {
            return [];
          }
        })
      );
  }

  getCountries(year): Observable<ICountry[]> {
    return this.httpService.get('country/year', `year=${year}`)
      .pipe(
        map(res => {
          if (res.success) {
            return res.data;
          } else {
            return [];
          }
        })
      );
  }
}
