import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

const url = 'http://127.0.0.1:3000';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(private http: HttpClient) {
  }

  get(path: string, param: string = ''): Observable<any> {
    return this.http.get<Observable<any>>(`${url}/${path}/?${param}`);
  }

  post(path: string, body = {}): Observable<any> {
    console.log(`${url}/${path}`)
    return this.http.post<Observable<any>>(`${url}/${path}`, body);
  }

}
