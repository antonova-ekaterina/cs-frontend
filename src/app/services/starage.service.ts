import {Injectable} from '@angular/core';

@Injectable()
export class StorageService {

  setItem = (name: string, value: string) => localStorage.setItem(name, value);
  getItem = (name: string) => localStorage.getItem(name);
  clear = () => localStorage.clear();

}
